export function init(context) {
  const backlink = context.querySelector('.back-link');
  if (!backlink) return;
  let hasHistory = false;
  if (document.referrer.split('/')[2] === window.location.host) {
    backlink.addEventListener('click', (event) => {
      event.preventDefault();
      history.back();
      setTimeout(function () {
        if (!hasHistory) {
          window.location.href = backlink.getAttribute('href');
        }
      }, 200);
    });
  }
  window.addEventListener('beforeunload', function () {
    hasHistory = true;
  });
}
