export const init = (context, settings) => {
  const nav = context.querySelector('.navigation');
  const openClass = 'navigation--open';

  const ariaOpenText = Drupal.t('Open navigation');
  const ariaCloseText = Drupal.t('Close navigation');

  if (nav) {
    const navToggle = nav.querySelector('.navigation__toggle');
    navToggle.addEventListener('click', () => {
      const open = navToggle.getAttribute('aria-expanded');
      if (open === 'true') {
        navToggle.setAttribute('aria-expanded', 'false');
        navToggle.setAttribute('aria-label', ariaOpenText);
        nav.classList.remove(openClass);
        document.body.classList.remove(openClass);
      } else {
        navToggle.setAttribute('aria-expanded', 'true');
        navToggle.setAttribute('aria-label', ariaCloseText);
        nav.classList.add(openClass);
        document.body.classList.add(openClass);
      }
    }, false);
  }
};
