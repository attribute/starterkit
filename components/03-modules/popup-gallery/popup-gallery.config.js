'use strict';

const faker = require('faker');

let images = [];

for (var i = 0; i < 4; i++) {
  images.push({
    photoswipe_data: '[{&quot;src&quot;:&quot;https://source.unsplash.com/random/1200x1600?sig=' + i + '&quot;,&quot;w&quot;:1920,&quot;h&quot;:2560}]',
    fallback: 'https://source.unsplash.com/random/1200x1600?sig' + i,
  });
}

module.exports = {
  context: {
    images: images,
  }
};
