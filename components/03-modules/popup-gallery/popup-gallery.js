/**
 * @file
 * Photoswipe initialization
 */

let $photoSwipeImages, $galleries, webPSupport;
const pswpElement = document.querySelectorAll('.pswp')[0];
import supportsWebP from 'supports-webp';
import * as util from '../../../js/misc/util';

const defaultOptions = {
  index: 0,
  history: false,
  showHideOpacity: true,
  bgOpacity: 1,
  shareEl: false,
  zoomEl: false,
  fullscreenEl: false,
  captionEl: true,
  counterEl: false,
  closeEl: true,
  preloaderEl: true,
  barsSize: {top: 0, bottom: 0},
  maxSpreadZoom: 1
};

const defaultImageStyle = 'default_xxxl';
const imageStyles = {
  default_s: 640,
  default_m: 960,
  default_l: 1280,
  default_xl: 1440,
  default_xxl: 1920,
  default_xxxl: 2560,
};
const multiplier = 1;

export function init(context) {
  supportsWebP.then(supported => {
    if (supported) {
      webPSupport = true;
    } else {
      webPSupport = false;
    }
  })
  $photoSwipeImages = context.querySelectorAll('.popup-gallery__item')
  $photoSwipeImages.forEach(($photoSwipeImage) => {
    $photoSwipeImage.addEventListener('click', handleImageClick)
  });

  $galleries = context.querySelectorAll('.popup-gallery--default')
  $galleries.forEach(($gallery) => {
    $gallery.addEventListener('click', handleGalleryClick)
  });
}

export function getImageUrl(fileName, imageStyle) {
  return `${drupalSettings.path.fileBaseUrl}/${imageStyle}/public/${fileName}`
}

export function handleImageClick(event) {
  event.preventDefault();
  event.stopPropagation();
  initPSWPImages(event.currentTarget)
}

export function handleGalleryClick(event) {
  event.preventDefault();
  event.stopPropagation();
  const gallery = event.target.closest('.popup-gallery');
  const firstImage = gallery.querySelector('.popup-gallery__item');
  initPSWPImages(firstImage);
}

export function initPSWPImages($image) {
  event.preventDefault();

  let items = [];

  // generate items array
  let $images = $image.parentNode.children;
  for (let element of $images) {
    const data = JSON.parse(element.dataset.photoswipe);
    for (const key in data) {
      const value = data[key];
      let msrc;
      let img = element.querySelector('img');
      let src;
      if (img.currentSrc !== undefined) {
        src = img.currentSrc;
      } else {
        src = img.src;
      }

      if(webPSupport === true) {
        value.fileName = `${value.fileName}.webp`;
      }

      if (!src.startsWith('data:')) {
        msrc = src;
      }

      const $caption = element.querySelector('figcaption');
      let caption = '';
      if ($caption.length) {
        caption = $caption[0].innerHTML;
      }

      items.push({
        fileName: value.fileName,
        msrc: msrc,
        src: msrc,
        w: value.w,
        h: value.h,
        imageElement: img,
        title: caption,
      });
    }
  }

  let options = defaultOptions;
  options.index = util.elementIndex($image);

  let gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
  handleResponsiveImages(gallery);
  gallery.init();
}

export function handleResponsiveImages(gallery) {
  // create variable that will store real size of viewport
  let realViewportWidth;
  let imageStyle = defaultImageStyle;
  let firstResize = true;
  let imageSrcWillChange;

  // beforeResize event fires each time size of gallery viewport updates
  // beforeResize event fires each time size of gallery viewport updates
  gallery.listen('beforeResize', function () {
    // gallery.viewportSize.x - width of PhotoSwipe viewport
    // gallery.viewportSize.y - height of PhotoSwipe viewport
    // window.devicePixelRatio - ratio between physical pixels and device independent pixels (Number)
    //                          1 (regular display), 2 (@2x, retina) ...

    var viewportSize = gallery.viewportSize.x > gallery.viewportSize.y ? gallery.viewportSize.x : gallery.viewportSize.y;

    // calculate real pixels when size changes
    realViewportWidth = viewportSize * multiplier;

    // Code below is needed if you want image to switch dynamically on window.resize
    for (let key in imageStyles) {
      if (realViewportWidth <= imageStyles[key]) {
        imageStyle = key;
        break;
      }
    }

    // Invalidate items only when source is changed and when it's not the first update
    if (imageSrcWillChange && !firstResize) {
      // invalidateCurrItems sets a flag on slides that are in DOM,
      // which will force update of content (image) on window.resize.
      gallery.invalidateCurrItems();
    }

    if (firstResize) {
      firstResize = false;
    }

    imageSrcWillChange = false;

  });


  // gettingData event fires each time PhotoSwipe retrieves image source & size
  gallery.listen('gettingData', function (index, item) {
    // Set image source & size based on real viewport width
    if (item.fileName.indexOf('.svg') === -1) {
      item.src = getImageUrl(item.fileName, imageStyle);
    }
  });
}
