export const init = () => {
  let $slideshows = document.querySelectorAll('.slideshow');

  if ($slideshows.length) {
    $slideshows.forEach(($slideshow) => {

      const $slideshowItems = $slideshow.querySelector('.slideshow__item-list');
      //const $counter = $slideshow.querySelector('.slideshow__counter');

      const flkty = new Flickity( $slideshowItems, {
        cellSelector: '.slideshow__item',
        //pageDots: false,
        wrapAround: true,
        selectedAttraction: 0.1,
        friction: 0.5
        //dragThreshold: 30
      });

      /*
      slideshow.querySelectorAll('img').bind('load', event => {
        flkty.resize()
      }).bind('click', function (event, pointer, cellElement, cellIndex) {
        flkty.next()
      });
      */

      /*
      flkty.on( 'select', () => {
        const newCounterState = `${flkty.selectedIndex + 1}/${flkty.cells.length}`;
        console.log($counter);
        $counter.innerHTML = newCounterState;
      });
      */
    });
  }
};
