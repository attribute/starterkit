/******************************************\
 * @file
 * A JavaScript file for the theme.
\******************************************/

import './misc/polyfills';
import * as lazySizes from './misc/lazysizes';
import * as navigation from 'components/03-modules/navigation/navigation';
import * as svgpolyfill from './misc/svgpolyfill';
import * as headroom from './misc/headroom';
//import * as popupGallery from 'components/03-modules/popup-gallery/popup-gallery';
//import * as gallery from 'components/03-modules/gallery/gallery';
//import * as accordion from 'components/03-modules/accordion/accordion';

//magnificMessages.init();


Drupal.behaviors.starterkit = {
  attach: (context, settings) => {
    navigation.init(context, settings);
    lazySizes.aspectRatio.init(context, settings);
    svgpolyfill.init();
    headroom.init(context);
    //popupGallery.init(context);
    //gallery.init();
    //accordion.init(context);
  }
};
