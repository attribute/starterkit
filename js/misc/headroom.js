import Headroom from "headroom.js";
let headroomInstance;

export function init(context) {
  if(!headroomInstance) {
    const options = {
      offset: 40,
      useTouchmove: true,
    }
    headroomInstance = new Headroom(document.body, options);
    headroomInstance.init();
  }
}
