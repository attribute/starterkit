export function init(context) {
  if (window.location.hash !== '') {
    const hash = window.location.hash.replace('#', '');
    const element = document.querySelector(`[name="${hash}"]`);
    if(element !== null) {
      element.focus();
    }
  }
}
