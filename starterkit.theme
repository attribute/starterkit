<?php

use Drupal\Core\Render\Element;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\image\Entity\ImageStyle;
use Drupal\media\IFrameMarkup;

/**
 * Implements hook_preprocess_HOOK().
 */
function starterkit_preprocess_input(array &$variables) {
  if (in_array($variables['element']['#type'], [
    'textfield',
    'email',
    'tel',
    'password',
    'number'
  ])) {
    if (!isset($variables['attributes']['placeholder'])) {
      $variables['attributes']['placeholder'] = $variables['element']['#title'];
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function starterkit_preprocess_textarea(array &$variables) {
  if (isset($variables['element']['#title'])) {
    $variables['attributes']['placeholder'] = $variables['element']['#title'];
  }
}

function starterkit_preprocess_links__language_block(array &$variables) {
  $language = \Drupal::languageManager()->getCurrentLanguage();
  foreach ($variables['links'] as $key => $link) {
    $variables['links'][$key]['text'] = $key;
    $variables['links'][$key]['link']['#title'] = $key;
    if ($key == $language->getId()) {
      $variables['links'][$key]['link']['#options']['attributes']['class'][] = 'is-active';
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function starterkit_preprocess_block(array &$variables) {
  $variables['attributes']['class'] = $variables['attributes']['id'];
  unset($variables['attributes']['id']);
}

/**
 * Implements hook_preprocess_HOOK().
 */
function starterkit_preprocess_field(array &$variables) {
  $variables['bundle'] = $variables['element']['#bundle'];
  $variables['view_mode'] = $variables['element']['#view_mode'];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function starterkit_theme_suggestions_form_alter(array &$suggestions, array $variables, string $hook) {
  $suggestions[] = 'form__' . $variables['element']['#form_id'];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function starterkit_theme_suggestions_field_alter(array &$suggestions, array $variables, string $hook) {
  // Add suggestions to fields on paragraph fields
  if ($variables['element']['#entity_type'] == 'paragraph') {
    array_unshift($suggestions, 'field__' . $variables['element']['#entity_type']);
    $paragraph = $variables['element']['#object'];
    $parent = $paragraph->getParentEntity();
    $bundle = $parent->bundle();
    $suggestions[] = 'field__' . $variables['element']['#entity_type'] . '__' . $variables['element']['#field_name'] . '__' . $variables['element']['#bundle'] . '__' . $bundle;
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function starterkit_theme_suggestions_views_view_alter(array &$suggestions, array $variables) {
  $suggestions[] = 'views_view__' . $variables['view']->id();
  $suggestions[] = 'views_view__' . $variables['view']->id() . '__' . $variables['view']->current_display;
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function starterkit_theme_suggestions_views_view_fields_alter(array &$suggestions, array $variables, string $hook) {
  $suggestions[] = 'views_view_fields__' . $variables['view']->id();
  $suggestions[] = 'views_view_fields__' . $variables['view']->id() . '__' . $variables['view']->current_display;
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function starterkit_theme_suggestions_views_view_unformatted_alter(array &$suggestions, array $variables, string $hook) {
  $suggestions[] = 'views_view_unformatted__' . $variables['view']->id();
  $suggestions[] = 'views_view_unformatted__' . $variables['view']->id() . '__' . $variables['view']->current_display;
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function starterkit_theme_suggestions_views_view_table_alter(array &$suggestions, array $variables, string $hook) {
  $suggestions[] = 'views_view_table__' . $variables['view']->id();
  $suggestions[] = 'views_view_table__' . $variables['view']->id() . '__' . $variables['view']->current_display;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function starterkit_preprocess_responsive_image(array &$variables) {
  $variables['img_element']['#width'] = $variables['width'];
  $variables['img_element']['#height'] = $variables['height'];

  if (isset($variables['sources'])) {
    /** @var \Drupal\Core\Template\Attribute $source */
    foreach ($variables['sources'] as $key => $source) {
      $variables['sources'][$key]['data-srcset'] = $source['srcset'];
      unset( $source['srcset']);
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function starterkit_preprocess_image(array &$variables) {
  $variables['attributes']['class'] = ['lazyload'];

  if (isset($variables['attributes']['sizes'])) {
    $variables['attributes']['data-sizes'] = $variables['attributes']['sizes'];
    unset($variables['attributes']['sizes']);
  }

  if (isset($variables['attributes']['srcset']) && !empty($variables['attributes']['srcset']->value())) {
    $variables['attributes']['data-srcset'] = $variables['attributes']['srcset'];
    unset($variables['attributes']['srcset']);
  }
  else if (isset($variables['attributes']['src'])) {
    $variables['attributes']['data-src'] = $variables['attributes']['src'];
  }

  if(isset($variables['width']) && isset($variables['height'])) {
    $variables['attributes']['data-aspectratio'] = round($variables['width'] / $variables['height'], 3);
  }

  $variables['fallback'] = $variables['attributes']['src'];
  $variables['attributes']['src'] = 'data:image/gif;base64,R0lGODlhAQABAIABAP///wAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';
  unset($variables['attributes']['loading']);
}

/**
 * Implements hook_preprocess_field__HOOK().
 */
function starterkit_preprocess_menu_local_tasks(array &$variables) {
  foreach (Element::children($variables['primary'], $sort = TRUE) as $key) {
    if($variables['primary'][$key]['#access'] instanceof \Drupal\Core\Access\AccessResultAllowed) {
      $link = $variables['primary'][$key];
      $variables['items'][$key] = $link;
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function starterkit_preprocess_media__image(array &$variables) {
  /** @var \Drupal\media\Entity\Media $media */
  $media = $variables['media'];
  $image = $media->get('field_media_image')[0];
  $file = $image->entity;
  $mime_type = $file->getMimeType();
  $uri = $file->getFileUri();
  if ($mime_type == 'image/svg+xml') {
    $variables['content']['field_media_image'] = [
      '#theme' => 'image',
      '#uri' => $uri,
      '#alt' => $image->alt,
    ];
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
/* function starterkit_preprocess_field__paragraph__field_images__gallery(array &$variables) {
  foreach ($variables['items'] as $key => $item) {
    $variables['items'][$key]['photoswipe_data'] = _starterkit_get_photoswipe_image($item['content']['#media']);
    $variables['items'][$key]['fallback'] = _starterkit_get_photoswipe_image_fallback($item['content']['#media']);
  }
} */

/**
 * Get PhotoSwipe settings for an array of images
 *
 * @param \Drupal\media\Entity\Media $media
 */

function _starterkit_get_photoswipe_image($media) {
  /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $image_item */
  $image_item = $media->get('field_media_image')[0];

  /** @var \Drupal\file\Entity\File $file */
  $file = $image_item->entity;

  /** @var array $dimensions */
  $dimensions = [
    'width' => $image_item->width,
    'height' => $image_item->height,
  ];

  $uri = $file->getFileUri();
  $image_style = ImageStyle::load('default_xxl');
  $image_style->transformDimensions($dimensions, $uri);

  $photoswipe_image = [
    [
      'uri' => $uri,
      'width' => $dimensions['width'],
      'height' => $dimensions['height'],
    ],
  ];
  return _starterkit_get_photoswipe_images_settings($photoswipe_image);
}

/**
 * Get PhotoSwipe settings for an array of images
 *
 * @param \Drupal\media\Entity\Media $media
 */

function _starterkit_get_photoswipe_image_fallback($media) {
  /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $image_item */
  $image_item = $media->get('field_media_image')[0];

  /** @var \Drupal\file\Entity\File $file */
  $file = $image_item->entity;

  $uri = $file->getFileUri();
  $image_style = ImageStyle::load('default_xxl');
  return $image_style->buildUrl($uri);
}

/**
 * Get PhotoSwipe settings for an array of images
 */
function _starterkit_get_photoswipe_images_settings($images) {
  $photoswipe_settings = [];
  foreach ($images as $image) {
    $photoswipe_settings = [_starterkit_get_photoswipe_image_settings($image['uri'], $image['width'], $image['height'])];
    $json = json_encode($photoswipe_settings, JSON_NUMERIC_CHECK);
    //$json = preg_replace('/"([a-zA-Z]+[a-zA-Z0-9_]*)":/','$1:', $json);
  }
  return $json;
}

/**
 * Get PhotoSwipe settings for an image
 */
function _starterkit_get_photoswipe_image_settings($uri, $width, $height) {

  $image_settings = [
    'fileName' => _starterkit_remove_file_base_url($uri),
    'w' => $width,
    'h' => $height,
  ];

  return $image_settings;
}

/**
 * Remove file base url
 */
function _starterkit_remove_file_base_url($url, $remove_additionally = '') {
  $file_base_url = 'public://';
  $url = str_replace($file_base_url, '', $url);
  if (stripos($url, '.tif') !== false) {
    $url .= '.jpg';
  }
  return $url;
}

/**
 * Get file base from url
 */
function _starterkit_get_file_base_url() {
  $file_base_url = PublicStream::baseUrl() . '/styles';
  return $file_base_url;
}

/**
 * Implements hook_preprocess_HOOK().
 */
/* function starterkit_page_attachments_alter(&$page) {
  $page['#attached']['drupalSettings']['path']['fileBaseUrl'] = _starterkit_get_file_base_url();
} */

/*function starterkit_preprocess_html(array &$variables) {
  if(!\Drupal::service('router.admin_context')->isAdminRoute()) {
    $variables['page']['#attached']['html_head_link'][] = [
      [
        'rel' => 'preload',
        'href' => '//hello.myfonts.net/count/3bf62a',
        'as' => 'style',
        'type' => 'text/css',
      ]
    ];

    $theme_path = \Drupal::theme()
      ->getActiveTheme()
      ->getPath();

    $fonts = [
      'font.woff2',
    ];

    foreach ($fonts as $font) {
      $url = '/' . $theme_path . '/fonts/' . $font;
      $variables['page']['#attached']['html_head_link'][] = [
        [
          'rel' => 'preload',
          'href' => $url,
          'as' => 'font',
          'type' => 'font/woff2',
          'crossorigin' => TRUE,
        ]
      ];
    }
  }
}*/

/**
 * Implements hook_form_HOOK_alter().
 */
/*
function starterkit_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  $children = Element::children($form);
  if ($form_id === 'views_exposed_form') {
    foreach (Element::children($form) as $key) {
      $element = $form[$key];
      if (isset($element['#type'])
        && in_array($element['#type'], ['radios', 'checkboxes'])
        && isset($element['#options']['All'])) {
        $form[$key]['#options']['All'] = t('All');
      }
    }
  }
}
*/

/**
 * {@inheritdoc}
 */
function starterkit_preprocess_media_oembed_iframe(array &$variables) {
  if (strpos((string) $variables['media'], 'youtube.com') !== FALSE) {
    $variables['media'] = IFrameMarkup::create(str_replace('youtube.com/', 'youtube-nocookie.com/', $variables['media']));
  }
}

/*
 * Implements hook_preprocess_field__HOOK().
 */
function starterkit_preprocess_form_element(array &$variables) {
  if ($variables['label_display'] === 'before') {
    if (in_array($variables['type'], [
      'textfield',
      'email',
      'tel',
      'password',
      'number',
      'select',
      'textarea',
    ])) {
      $variables['label_display'] = 'after';
    }
  }
}

/**
 * Implements HOOK_preprocess_email_wrap().
 */
function starterkit_preprocess_email_wrap(&$variables) {
  $variables['base_url'] = $GLOBALS['base_url'];
}
