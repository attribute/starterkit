// Webpack config
const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");

const baseConfig = {
  output: { filename: 'main.js' },
  resolve: {
    alias: {
      components: path.resolve(__dirname, 'components/'),
      '~': path.resolve(__dirname, '.'),
    },
  },
}

module.exports = {
  develop: {
    ...baseConfig,
    mode: 'development',
    devtool: 'source-map',
  },
  production: {
    ...baseConfig,
    mode: 'production',
    optimization: {
      minimize: true,
      minimizer: [
        new TerserPlugin(),
      ],
    },
  },
};
